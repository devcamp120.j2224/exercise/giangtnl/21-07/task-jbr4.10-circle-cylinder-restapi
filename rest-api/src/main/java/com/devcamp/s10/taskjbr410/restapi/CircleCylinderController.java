package com.devcamp.s10.taskjbr410.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CircleCylinderController {
    @CrossOrigin
    @GetMapping("/circle-area")
        public double getAreaCircle( @RequestParam(value="radius",defaultValue = "1") double radius){
            Circle circle1 = new Circle(radius);
            return circle1.getArea(); 
        }
    @GetMapping("/cylinder-volume")
        public double getVolumeCylinder( @RequestParam(value="radius",defaultValue = "1") double radius, 
        @RequestParam(value="height",defaultValue = "1") double height){
            Cylinder circle1 = new Cylinder(radius,height);
            return circle1.getVolume(); 
        }
        
}
